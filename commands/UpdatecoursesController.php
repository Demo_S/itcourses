<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Courses;
/**
 * This command grabs correspondig webpage contents and tries to parse it and update given course or all of them
 *
 * @author Dmytro Selin <demosglok@gmail.com>
 * @since 2.0
 */
//http://www.yiiframework.com/doc-2.0/guide-tutorial-console.html
class UpdatecoursesController extends Controller
{
    private function parse_spalah($document){
	    $descr = $document->find("#main .block-description");
	    var_dump($descr->text());
	    $length = $document->find('#main .media-list>li:nth-child(2) .media-body')->text();
	    var_dump(trim($length));
	    $num_classes = $document->find('#main .media-list>li:nth-child(3) .media-body')->text();
	    var_dump(trim($num_classes));
	    $full_descr = $document->find('#course .container .block-row');
	    var_dump($full_descr->text());
	    $price = $document->find('#price .container .text-center .block-title')->text();
	    $price = trim($price);
	    var_dump($price);
	    $registration = $document->find('#register .block-inner .container .page-header')->text();
	    $registration = trim($registration);
	    var_dump($registration, $registration == 'НАБОР ЗАКРЫТ');
    }
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($id=null)
    {
	if(is_null($id)){
    	    echo "id is null\n";
	}
	else{
	    echo "id=$id\n";
	    $course = Courses::findOne($id);
	    echo $course->name.' '.$course->url."\n";
	    if($course->url){
		$content = file_get_contents($course->url);
	    }
	    //load course, get its url, load url, parse url, get data from url
	    $document = \phpQuery::newDocumentHTML($content);
	    if(strpos($course->url,'spalah.ua') !== false){
		$this->parse_spalah($document);
	    }
	    //http://www.webapplex.ru/parsing-vneshnego-sajta-na-yii-2.x
	}
    }
}
