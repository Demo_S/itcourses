<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Courses */

$this->title = 'Create Courses';
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::a(Yii::t('app','Back to courses list'),['admin'],['class'=>'btn btn-info']);?>
    <?= $this->render('_form', [
        'model' => $model,'vocabularies'=>$vocabularies
    ]) ?>

</div>
