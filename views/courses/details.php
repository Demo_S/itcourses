<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Courses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	<?= Html::a(Yii::t('app','Back to courses list'), ['index'],['class'=>'btn btn-info'])?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
	    ['label'=>Yii::t('app','City'),'value'=>$model->city->name],
            'name',
            'description:ntext',
            ['label'=>Yii::t('app','Section'), 'value'=>$model->section->name],
            ['label'=>Yii::t('app','Company'), 'value'=>$model->company->name],
	    ['label' => Yii::t('app','Start'), 'value'=>$model->start > 0 ? date('d-m-Y',$model->start): Yii::t('app','As soon as group formed')],
            'schedule',
            'keywords',
	    'url:url',
	    ['label'=>Yii::t('app','categories'), 'value'=>implode(', ',ArrayHelper::getColumn($model->categories,function($elem){return $elem->name;}))],
	    ['label'=>'programmingLanguages','value'=>implode(', ',ArrayHelper::getColumn($model->programmingLanguages,function($elem){return $elem->name;}))],
        ],
    ]) ?>

</div>
