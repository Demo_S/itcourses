<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Courses */

$this->title = 'Update Courses: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="courses-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::a(Yii::t('app','Back to courses list'),['admin'],['class'=>'btn btn-info']);?>
    <?= $this->render('_form', [
        'model' => $model,'vocabularies'=>$vocabularies
    ]) ?>

</div>
