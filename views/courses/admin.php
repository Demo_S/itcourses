<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Courses', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['label'=>Yii::t('app','City'),'value'=>'city.name'],
            ['label'=>Yii::t('app','Section'),'value'=>'section.name'],
            ['label'=>Yii::t('app','Company'),'value'=>'company.name'],
            ['label'=>Yii::t('app','Name'), 'value'=>function($data){return $data->url ? Html::a($data->name,$data->url) : $data->name;},'format'=>'raw'],
            //'description:ntext',
            'start:datetime',
            // 'schedule',
            // 'keywords',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
