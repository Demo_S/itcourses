<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use execut\widget\TreeView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/site_additional.css');
$this->title = Yii::t('app','IT courses list');
$city = isset($this->params['clientCity']) ? $this->params['clientCity'] : '';
if(isset($city) && $city != false){
    $city = UCFirst($city);
    $this->title .= ' '.Yii::t('app','in '.$city);
}
$this->params['breadcrumbs'][] = $this->title;
$isMobile = \Yii::$app->mobileDetect->isMobile();

?>
<script>
    var baseUrl = '<?=Url::to(Yii::$app->requestedRoute,true)?>';
</script>
<div class="courses-index">
	<?php  if($cityGuessed) echo '<div class="alert alert-info">'. Yii::t('app','Your city was detected as').' '.Yii::t('app',$city) .', '. Yii::t('app','you can select another city from list' ). '</div>';?>
	<div class="row">
            <h1><?= Html::encode($this->title) ?></h1>
	</div>
	<div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3">
	    <div class="panel panel-default sidebar-categories">
		<div class="panel-heading" role="tab" id="category_tree_header">
		    <a  data-toggle="collapse" href="#category_tree_wrapper" aria-expanded="true" aria-controls="category_tree_wrapper" class="category_header_control <?=$isMobile?'collapsed':''?>">
			<span class="glyphicon glyphicon-filter"></span> <?=Yii::t('app','Filter by category')?> <span id="chevron" class="chevron glyphicon"></span>
		    </a>
		</div>
	    
	    
	    <div id="category_tree_wrapper" class="category_tree_wrapper panel-collapse collapse <?=!$isMobile?'in':''?> " role="tabpanel" aria-labelledby="category_tree_header">
	    <div class="panel-body">
	    <?php foreach($teachCategories as $category_name => $category_entries):?>
		<div class="panel panel-info">
		    <div class="panel-heading">
			<div class="panel-title">
			    <?=Yii::t('app',$category_name)?>
			</div>
		    </div>
			<div class="list-group category_entries" aria-label="<?=$category_name?>" data-category-name="<?=$category_name?>" >
			    <?php foreach($category_entries as $entry_id => $entry):?>
				<?php if(is_array($entry)):?>
				    <button class="btn btn-default list-group-item collapsed" data-toggle="collapse" data-target="#<?=$category_name?>_subcategory" autocomplete="off">
					<?=$entry['name']?><span class="glyphicon chevron"></span>
				    </button>
				    <div class="collapse subcategory" id="<?=$category_name?>_subcategory">
				    <?php foreach($entry['subcategories'] as $subcategory_id => $subcategory):?>
					<?php $selected = isset($selectedCategories[$category_name]) && is_array($selectedCategories[$category_name]) ? in_array($subcategory_id,$selectedCategories[$category_name]) : false;
					    $aria = $selected ? ' aria-pressed=true ' : '';
					    $active = $selected ? ' active ':'';
					?>
					<button class="btn btn-default list-group-item <?=$active?>" data-toggle="button" autocomplete="off" data-category="<?=$subcategory_id?>" <?=$aria?> >
					    <i class="glyphicon"></i><?=$subcategory?>
					</button>
				    <?php endforeach?>
				    </div>
				<?php else:?>
				    <?php $selected = isset($selectedCategories[$category_name]) && is_array($selectedCategories[$category_name]) ? in_array($entry_id,$selectedCategories[$category_name]) : false;
					$aria = $selected ? ' aria-pressed=true ' : '';
					$active = $selected ? ' active ':'';
				    ?>
				    <button class="btn btn-default list-group-item <?=$active?>" data-toggle="button" autocomplete="off" data-category="<?=$entry_id?>" <?=$aria?>>
					<i class="glyphicon"></i><?=$entry?>
				    </button>
				<?php endif?>
			    <?php endforeach;?>
			</div>
		</div>
	    <?php endforeach;?>
            <?php

	    $this->registerJs(<<<BUTTONS_HANDLER
		$(function(){
		    $('#category_tree_wrapper button.btn').click(function(e){
		    setTimeout(function(){//to avoid race conditio when bootstrap sets 'active' class to button
			let selected_categories = [];
			$('#category_tree_wrapper .category_entries').each(function(num,category){
			    let category_name = category.getAttribute('data-category-name');
			    let selected_buttons = [];
			    $(category).find('.active').each(function(num1, button){
				selected_buttons.push(button.getAttribute('data-category'));
			    });
			    if(selected_buttons.length > 0){
				selected_categories[category_name] = selected_buttons;
			    }
			});
			
			updateGrid(selected_categories);
			
		    },300);
		    });
		});
BUTTONS_HANDLER
	    );

	    $this->registerJs(<<<JS

		function updateGrid(selected_categories){
		    let selected_categories_url='';
		    for(let category in selected_categories){
			    selected_categories_url += '&'+category+'='+selected_categories[category].join();
		    }
		    
		    $.pjax({
    			container: '#courses-tree-pjax-wrapper',
    			url: baseUrl + '?'+selected_categories_url,
    			timeout: 3000
		    });
		}
JS
	    );
            ?>
	    </div>
	    </div>
	    </div>
        </div>
        <div class="col-xs-12 col-md-9 col-sm-9">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php Pjax::begin(['id'=>'courses-tree-pjax-wrapper']); ?> 
		<?php /*http://demos.krajee.com/grid*/?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
		    'filterPosition' => $isMobile ? null : GridView::FILTER_POS_BODY,
		    'showHeader' => !$isMobile,
		    'tableOptions'=>['class'=>'rounded-table'],
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        //'city_id',
                        /*[   'attribute' => 'section_id',
                            'format' => 'html',
                            'value' => function ($model) {return "val";},
                            'filter' => Html::activeDropDownList(
                                    $searchModel,
                                    'section_id',
                                    $sections,
                                    ['class' => 'form-control', 'prompt' => \Yii::t('app', 'Select section')]
                            )
                            //'filter' => Html::activeDropDownList($searchModel, 'attribute_name', ArrayHelper::map(ModelName::find()->asArray()->all(), 'ID', 'Name'),['class'=>'form-control','prompt' => 'Select Category']),

                        ],*/
			['label'=>Yii::t('app','Company'),'value'=>'company.name'],
                        ['label' => Yii::t('app','Name'), 'format'=>'html','value'=>function($data){return Html::a($data->name,['courses/details','id'=>$data->id]);}],
                         //'description:ntext',
                         ['attribute'=>'start','format'=>'html','value'=>function($model){return $model->start < 1000 && strlen($model->start_comment)>0 ? $model->start_comment : date('d-m-y',$model->start);}],
                         'schedule',
                         ['attribute'=>'keywords','value'=>function($model){return str_replace(',',', ',$model->keywords);}],
                         ['attribute'=>'price','headerOptions'=>['style'=>'width:9.5%']],

                        ['class' => 'yii\grid\ActionColumn','buttons'=>['details'=>function($url,$model,$key){return Html::a(Yii::t('app','Description'),$url,['class'=>'btn btn-success']);}],'template'=>'{details}'],
                    ],
		    'export'=>false,
//		    'responsive' => true,
		    'responsiveWrap' => true
                ]); ?>
	    <!-- end gridview widget -->
            <?php Pjax::end(); ?>
        </div>
	</div>

</div>