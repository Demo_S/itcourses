<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form container">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->dropDownList($vocabularies['cities'],['prompt'=>Yii::t('app','Select City')]) ?>

    <?= $form->field($model, 'section_id')->dropDownList($vocabularies['sections'],['prompt'=>Yii::t('app','Select Section')]) ?>

    <?= $form->field($model, 'company_id')->dropDownList($vocabularies['companies'],['prompt'=>Yii::t('app','Select Company')]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start_str')->widget(\nkovacs\datetimepicker\DateTimePicker::classname(),['format'=>'yyyy-MM-dd HH:mm',
												    'clientOptions'=>['extraFormats'=>['yyyy-MM-dd HH:mm']]
												    ]) ?>
    <?= $form->field($model, 'start_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'schedule')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price_personal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    <div class="row">
	<div class="col-xs-6 col-sm-3">
		<?= $form->field($model, 'whoteach_ids')->dropDownList($vocabularies['whoteach'],['multiple'=>true,'size'=>6]) ?>
	</div>
	<div class="col-xs-6 col-sm-3">
		<?= $form->field($model, 'whomteach_ids')->dropDownList($vocabularies['whomteach'],['multiple'=>true,'size'=>6]) ?>
	</div>
	<div class="col-xs-6 col-sm-3">
		<?= $form->field($model, 'whatteach_ids')->dropDownList($vocabularies['whatteach'],['multiple'=>true,'size'=>6]) ?>
	</div>
	<div class="col-xs-6 col-sm-3">
		<?= $form->field($model, 'howteach_ids')->dropDownList($vocabularies['howteach'],['multiple'=>true,'size'=>6]) ?>
	</div>
    </div>
    <div class="row">
	<div class="col-xs-6">
	<?= $form->field($model, 'categories_ids')->dropDownList($vocabularies['categories'],['multiple'=>true,'size'=>20]) ?>
	</div>
	<div class="col-xs-6">
	<?= $form->field($model, 'programminglanguages_ids')->dropDownList($vocabularies['programming_languages'],['multiple'=>true, 'size'=>20]) ?>
	</div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Back'),Yii::$app->request->referrer,['class'=>'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
