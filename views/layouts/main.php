<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\TopmenuEntitiesComponent;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php 
	$city = isset($this->params['clientCity']) ? $this->params['clientCity'] : '';
	$city = UCFirst($city);
	$section = isset($this->params['clientSection']) ? $this->params['clientSection'] : 'All';
    ?>
    <title><?= Html::encode($this->title).' '.$city ?></title>
    <?php $this->head() ?>
</head>
<body data-spy="scroll" data-offset="80">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php 
    NavBar::begin([
        'brandLabel' => 'IT Courses '.$city,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $cities = TopmenuEntitiesComponent::getCities();
    $sections = TopmenuEntitiesComponent::getSections();
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Направление: '.Yii::t('app',$section),
                'items' => $sections,
            ],
            ['label' => 'Город '.Yii::t('app',$city),
                'items' => $cities,
            ],
//            ['label' => 'Курсы', 'url' => ['/courses/index']],
            ['label' => 'Про нас', 'url' => ['/site/about']],
            ['label' => 'Вопросы', 'url' => ['/site/faq']],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
            ['label' => 'Главная', 'url' => ['/site/index']],
/*
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
*/
        ],
    ]);
    NavBar::end();
    ?>
    <div id="home" >
            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
    </div>

</div>

<footer class="footer" id="footer2">
    <?php echo $this->render('footer');?>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
