<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2">
        	<p class="pull-left">&copy; ITCourses <?= date('Y') ?></p>
		<div class="clearfix"></div>
		<div class="icons-credits">Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
            </div>
            <div class="col-xs-5 col-sm-2 col-md-2 bottom-link-block">
		<?=Html::a('Киев','//kiev.itcourses.com.ua/courses/index')?>
		<?=Html::a('Днепр','//dnepr.itcourses.com.ua/courses/index')?>
		<?=Html::a('Харьков','//kharkov.itcourses.com.ua/courses/index')?>
		<?=Html::a('Одесса','//odessa.itcourses.com.ua/courses/index')?>
		<?=Html::a('Львiв','//lviv.itcourses.com.ua/courses/index')?>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-3 bottom-link-block">
                <a href="#programming">Программирование</a>
                <a href="#testing">Тестирование</a>
                <a href="#design">Дизайн</a>
                <a href="#system_administration">Администрирование</a>
                <a href="#management">Менеджмент</a>
                <a href="#hr">HR</a>
                <a href="#hr">SEO</a>
            </div>
            <div class="col-xs-5 col-sm-2 col-md-2 bottom-link-block">
		<?=Html::a(Yii::t('app','FAQ'),'/site/faq')?>
		<?=Html::a(Yii::t('app','About'),'/site/about')?>
                <a href="#blog">Статьи</a>
		<?=Html::a(Yii::t('app','Subscribe'),'/site/subscribe')?>
		<?=Html::a(Yii::t('app','Contact'),'/site/contact')?>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-2">
                        <a href="https://www.facebook.com/groups/itcourses.com.ua" class="social-icon"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/ITcourses_ua" class="social-icon"><i class="fa fa-twitter"></i></a>
                        <a href="https://vk.com/itcourses_com_ua" class="social-icon"><i class="fa fa-vk"></i></a>
                        <a href="https://t.me/itcourses_com_ua" class="social-icon"><i class="fa fa-telegram"></i></a>
            </div>
        </div>
    </div>

<script>
  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/89798/script.js');
</script>