<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body data-spy="scroll" data-offset="80">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'IT Courses',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Каталог курсов', 'url' => ['/courses/index']],
            ['label' => 'Про нас', 'url' => ['/site/about']],
            ['label' => 'Вопросы', 'url' => ['/site/faq']],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
/*
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
*/
        ],
    ]);
    NavBar::end();
    ?>
			<a class="downarrow" href="#">
			    <div class="arrow-inner"></div>
			</a>

    <div id="home" class="top-content">
        <div class="inner-bg">
	
            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                        <h2>Подбери себе компьютерные курсы</h2>
			<div class="description">
			    <ul class="col-sm-5 point-lines">
				<li>Хотите "войти в АйТи" и нужно <b>определиться с чего начать</b> ?</li>
				<li>Хотите выбрать <b>компьютерные курсы</b>, которые введут Вас в курс дела ?</li>
				<li>Хотите <b>повысить квалификацию</b> и ищете подходящие курсы для этого ?</li>
				<li>Нужна помощь или совет как <b>устроится на первое место работы</b> ?</li>
			    </ul>
			    <div class="col-sm-5 col-sm-offset-1">
			    <p>Мы собрали лучше компьютерные курсы по крупным городам Украины в одном месте и представили их в удобном для поиска и сравнения
			    виде.  А так же масса статей советов на смежные темы , от выбора направления, языка и технологии до помощи в составлении резюме и 
			    устройстве на первую работу</p>
			    </div>
                    	    <div class="hint">Этот сайт - попытка собрать в одном месте все многообразие компьютерных курсов, появившихся
                        	последнее время. Так что б при этом можно было поискать по направлениям, сравнить расписания, длительность и цену.
                        	В дальнейшем, надеемся, можно и качество на основе отзывов будет сравнивать
                    	    </div>
			</div>
                        <div class="top-button">
                            <a class="btn btn-top btn-lg" href="#features-section">Каталог курсов</a>
                            <a class="btn btn-top btn-lg" href="#"> Курсы для детей</a>
                            <a class="btn btn-top btn-lg" href="#"> Стажировки от компаний</a>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div id="features-section" class="features-container">
        <?= $content ?>
    </div>

</div>

<footer class="footer" id="footer2">
<?php echo $this->render('footer');?>
</footer>

<?php $this->endBody() ?>
<?php 
$this->registerJs('
$(function () {
  var w = $(window);
  w.scroll(function () {
    if (w.scrollTop() !== 0) {
      $(".downarrow").removeClass("visible");
      return;
    }
    
    $(".downarrow").addClass("visible");
  });
  
  $(".downarrow").addClass("visible");
  
  
  $(".downarrow").click(function(e){
    e.preventDefault();
    var top = w.scrollTop()+w.height();
    w.scrollTop(top);
  });
});
');
?>
</body>
</html>
<?php $this->endPage() ?>
