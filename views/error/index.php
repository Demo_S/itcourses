<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ошибка');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="error-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?=Html::encode($this->message)?></p>
</div>