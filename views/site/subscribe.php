<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','Subscribe');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('subscribeFormSubmitted')): ?>
	<?php $flash = Yii::$app->session->getFlash('subscribeFormSubmitted');
	if($flash == 'success'){?>
        <div class="alert alert-success">
	    <?=Yii::t('app','Thank you for subscribing our newsletter. We will try our best to be informative and unobtrusive')?>
        </div>
	<?php } else {?>
        <div class="alert alert-danger">
	    <?=$flash?>
        </div>

	<?php }?>

    <?php else: ?>

        <p>
	    <?=Yii::t('app','Please provide your email to subscribe to our newsletter')?>
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'subscribe-form']); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app','Subscribe'), ['class' => 'btn btn-primary', 'name' => 'subscribe-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    <?php endif; ?>
	<div class="row social-links">
	    <div class="col-sm-5">
		<p><?=Yii::t('app','Please find us in social networks')?> </p>
		<div>
		<a href="https://www.facebook.com/groups/itcourses.com.ua/?ref=group_cover" target="_blank" class="social-icon2 social-icon-fb"></a>
		<a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Fdnepr.itcourses.com.ua%2Fsite%2Fsubscribe&ref_src=twsrc%5Etfw&region=follow_link&screen_name=ITcourses_ua&tw_p=followbutton"
		    target = "_blank" class="social-icon2 social-icon-twitter"></a>
		<a href="https://vk.com/itcourses_com_ua" target="_blank" class="social-icon2 social-icon-vk"></a>
		</div>
	    </div>
	    <div class="col-sm-1">
	    </div>
	    <div class="col-sm-5">
		<p><?=Yii::t('app','Please join our Telegram channel')?></p>
		<?=Html::a('','http://t.me/itcourses_com_ua',['class'=>'social-icon2 social-icon-telegram'])?>
	    </div>
	</div>
</div>
