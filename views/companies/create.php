<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Companies */

$this->title = 'Create Companies';
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companies-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::a(Yii::t('app','Back to companies list'),['index'],['class'=>'btn btn-info']);?>
    <?= $this->render('_form', [
        'model' => $model,
	'cities' => $cities
    ]) ?>

</div>
