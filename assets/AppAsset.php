<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        "http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800",
        "http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700",
        "assets/font-awesome/css/font-awesome.min.css",
        'css/style.css'
    ];

    public $js = [
        "assets/js/jquery.backstretch.min.js",
        "assets/js/retina-1.1.0.min.js",
        "assets/js/jquery.easing.1.3.min.js",
        "assets/js/main.js"

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
