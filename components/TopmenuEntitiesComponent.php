<?php
namespace app\components;

use yii\base\Object;
use app\models\Cities;
use app\models\Sections;
use yii\helpers\Url;

class TopmenuEntitiesComponent extends Object
{

    public function getCities()
    {
	$models = Cities::find()->all();
	$cities = [];
	foreach($models as $model){
	    $cities[] = ['label'=>$model->name,'url'=>['/courses/index','city'=>$model->urlprefix]];
	}
        return $cities;
    }
    public function getSections()
    {
	$models = Sections::find()->all();
	$sections = [0=>['label'=>'All','url'=>['/courses/index','section'=>0]]];
	foreach($models as $model){
	    $sections[] = ['label'=>$model->name,'url'=>['/courses/index','section'=>$model->id]];
	}
        return $sections;
    }
}