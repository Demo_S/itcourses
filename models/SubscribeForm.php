<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SubscribeForm extends Model
{
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email are required
            [ ['email' ], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
        ];
    }
    public function confirm($emailfrom,$namefrom)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom([$emailfrom => $namefrom])
                ->setSubject('Subscription to Itcourses.com.ua news')
                ->setTextBody("You was successfully subscribed to our newsletter\n\nSincerely yours\nItcourses team")
                ->send();

            return true;
        }
        return false;
    }

}
