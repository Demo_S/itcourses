<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "whatteach".
 *
 * @property integer $id
 * @property string $name
 * @property string $comment
 */
class Whatteach extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'whatteach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 250],
            [['comment'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'comment' => Yii::t('app','Comment'),
        ];
    }
    public function getCourses()
    {
	$this->hasMany(Courses::className(),['id'=>'course_id'])->viaTable('courses_whatteach',['whatteach_id'=>'id']);
    }
}
