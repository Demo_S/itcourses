<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property integer $id
 * @property string $name
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','website'], 'string', 'max' => 250],
	    [['cities_ids'],'each','rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'website' => Yii::t('app','Web Site'),
	    'cities_ids' => Yii::t('app', 'Cities')
        ];
    }
    public function behaviors()
    {
	return [
	    [
		'class' => \voskobovich\linker\LinkerBehavior::className(),
		'relations' => [
		    'cities_ids' => 'cities',
		]
	    ]
	];
    }

    public function getCities()
    {
	return $this->hasMany(Cities::className(),['id'=>'city_id'])->viaTable('companies_cities',['company_id'=>'id']);
    }
}
