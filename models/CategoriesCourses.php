<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories_courses".
 *
 * @property integer $category_id
 * @property integer $course_id
 */
class CategoriesCourses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id, course_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('app','Category'),
            'course_id' => Yii::t('app','Course'),
        ];
    }

    public function getCourse()
    {
	return $this->hasOne(Courses::className(),['id'=>'course_id']);
    }
    public function getCategory()
    {
	return $this->hasOne(Categories::className(),['id'=>'category_id']);
    }
}
