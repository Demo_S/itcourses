<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Courses;
use app\models\TeachCategories;

/**
 * CoursesSearch represents the model behind the search form about `app\models\Courses`.
 */
class CoursesSearch extends Courses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'section_id', 'company_id'], 'integer'],
            [['name', /*'description', 'schedule', */'keywords','categories'/*,'start'*/], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Courses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => isset($params['city_id'])?$params['city_id']:null,
            'section_id' => isset($params['section_id'])?$params['section_id']:null,
            'company_id' => $this->company_id,
            //'start' => $this->start,//start > $this->start
        ]);
	if(isset($params['whatteach']) && $params['whatteach'] && count($params['whatteach']) > 0){
		//select which params['whatteach'] is greater then 100;
		$programming_subcategories = array_filter($params['whatteach'], function($x){return $x > 100;});
		if(count($programming_subcategories) > 0){
		    $query->joinWith('programmingLanguages');
		    $condition = ['programming_language_id'=>$programming_subcategories];
		    $whatteach_categories = array_filter($params['whatteach'], function($x){return $x < 100;});
		    if(count($whatteach_categories) > 0){//add join with whatteach only if have whatteach different from programming subcats
			$query->joinWith('whatteach');
			$condition = ['or',$condition,['whatteach_id' => $whatteach_categories]];
		    }
		    $query->andWhere($condition);
		}
		else{
		    $query->joinWith('whatteach')->andWhere(['whatteach_id'=>$params['whatteach']]);
		}
	}
	if(isset($params['whoteach']) && $params['whoteach'] && count($params['whoteach']) > 0){
		$query->joinWith('whoteach')->andWhere(['whoteach_id'=>$params['whoteach']]);
	}
	if(isset($params['whomteach']) && $params['whomteach'] && count($params['whomteach']) > 0){
		$query->joinWith('whomteach')->andWhere(['whomteach_id'=>$params['whomteach']]);
	}
	if(isset($params['howteach']) && $params['howteach'] && count($params['howteach']) > 0){
		$query->joinWith('howteach')->andWhere(['howteach_id'=>$params['howteach']]);
	}
	$now = time();
	if(isset($params['timeteach']) && $params['timeteach'] && count($params['timeteach']) > 0){
		if(count($params['timeteach']) == 1){
			if($params['timeteach'][0] == TeachCategories::TIMETEACH_STARTED){
				$query->andWhere(['OR','start=0','start <= '.$now]);
			}
			else{//$params['timeteach'][0] == TeachCategories::TIMETEACH_FUTURE
				$query->andWhere(['OR','start=0','start > '.$now]);//default behaviour
			}
		}
		else{
			//both checked, do nothig with this clause, select all
		}
	}
	else{
		//select only unstarted courses by default, those with start date in future or <1000 (1970 year)		
		$query->andWhere(['OR','start=0','start > '.$now]);
	}

        $query->andFilterWhere(['like', 'name', $this->name])
        //    ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'schedule', $this->schedule])
            ->andFilterWhere(['like', 'keywords', $this->keywords]);

        return $dataProvider;
    }
}
