<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string $title
 * @property string $category
 * @property string $content
 * @property integer $ts
 * @property string $author
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['ts'], 'integer'],
            [['title', 'category', 'author','shortlink'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'category' => Yii::t('app', 'Category'),
            'content' => Yii::t('app', 'Content'),
            'ts' => Yii::t('app', 'Ts'),
            'author' => Yii::t('app', 'Author'),
            'shortlink' => Yii::t('app', 'Shortlink'),
        ];
    }
}
