<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "programming_languages".
 *
 * @property integer $id
 * @property string $name
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 250],
	    [['parent_id'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
	    'parent_id' => Yii::t('app','Parent category'),
        ];
    }
    public function getCategoryCourses()
    {
	return $this->hasMany(CategoriesCourses::className(), ['category_id'=>'id']);
    }
    public function getCourses()
    {
	return $this->hasMany(Courses::className(),['id'=>'course_id'])->via('categoryCourses');
    }
}
