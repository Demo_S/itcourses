<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses_programming_languages".
 *
 * @property integer $course_id
 * @property integer $programming_language_id
 */
class CoursesProgrammingLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses_programming_languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id, programming_language_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => Yii::t('app','Course'),
            'programming_language_id' => Yii::t('app','Programming Language'),
        ];
    }

    public function getCourse()
    {
	return $this->hasOne(Courses::className(),['id'=>'course_id']);
    }
    public function getProgrammingLanguage()
    {
	return $this->hasOne(ProgrammingLanguages::className(),['id'=>'programming_language_id']);
    }
}
