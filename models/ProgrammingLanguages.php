<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "programming_languages".
 *
 * @property integer $id
 * @property string $name
 */
class ProgrammingLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'programming_languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
        ];
    }
    public function getCoursesWithProgrammingLanguage()
    {
	$this->hasMany(CoursesProgrammingLanguages::className(),['programming_language_id'=>'id']);
    }
    public function getCourses()
    {
	$this->hasMany(Courses::className(),['id'=>'course_id'])->via('coursesWithProgrammingLanguage');
    }
}
