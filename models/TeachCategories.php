<?php

namespace app\models;

class TeachCategories
{
	const TIMETEACH_FUTURE = 41;
	const TIMETEACH_STARTED = 42;
	const TIMETEACH_ENDED = 43;
	const CATEGORIES = ['whatteach'=>[
		    1 => ['name'=>'Программирование', 
			    'subcategories'=>[
				1   => 'Все нижеперечисленное',
				101 => 'Web (html, css, javascript)',
				102 => 'Mobile',
				103 => 'Javascript',
				104 => 'PHP',
				105 => 'C#',
				106 => 'C++',
				107 => 'Java',
				199 => 'Другое'
			    ]
			 ],
		    2 => 'Тестирование(QA)',
		    3 => 'Дизайн',
		    4 => 'Администрирование',
		    5 => 'Управл. персоналом(HR)',
		    6 => 'Проектный менеджмент',
		    7 => 'SEO, SMM, продвижение',
		    8 => 'Комп грамотность',
		    9 => 'Железо и роботы',
		    10 => 'Другое',
		],
		'whoteach'=>[
		    11 => 'Курсы от школ',
		    12 => 'Курсы от IT компаний',
		    13 => 'Онлайн курсы',
		    14 => 'Стажировки'
		],
		'whomteach'=>[
		    21 => 'Курсы для взросых',
		    22 => 'Курсы для детей',
		],
		'howteach'=>[
		    31 => 'С нуля',
		    32 => 'Для тех кто что-то знает'
		],
		'timeteach' => [
		    self::TIMETEACH_FUTURE => 'Еще не начавшиеся',
		    self::TIMETEACH_STARTED => 'Уже стартовавшие',
		    //TIMETEACH_ENDED => 'Уже завершившиеся',
		]
	      ];
}
