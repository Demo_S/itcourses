<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "whoteach".
 *
 * @property integer $id
 * @property string $name
 */
class Whoteach extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'whoteach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 250],
            [['comment'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'comment' => Yii::t('app','Comment'),
        ];
    }
    public function getCourses()
    {
	$this->hasMany(Courses::className(),['id'=>'course_id'])->viaTable('courses_whoteach',['whoteach_id'=>'id']);
    }
}
