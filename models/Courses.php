<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $section_id
 * @property integer $company_id
 * @property string $name
 * @property string $description
 * @property integer $start
 * @property string $start_comment
 * @property string $duration
 * @property string $schedule
 * @property string $keywords
 * @property string $price
 * @property string $price_personal
 */
class Courses extends \yii\db\ActiveRecord
{
    public $start_str;
    private $ICUtimeformat = 'yyyy-MM-dd HH:mm';
    private $PHPtimeformat = 'Y-m-d H:i';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'section_id', 'company_id'], 'integer'],
            [['name', 'city_id','section_id', 'company_id'], 'required'],
	    [['whoteach_ids', 'whomteach_ids', 'whatteach_ids', 'howteach_ids', 'categories_ids', 'programminglanguages_ids'], 'each', 'rule' => ['integer']],
            [['description','url'], 'string'],
            [['name', 'start_comment', 'schedule', 'keywords','url'], 'string', 'max' => 250],
            [['duration','price','price_personal'], 'string', 'max' => 200],
	    [['start_str'],'datetime','format'=>$this->ICUtimeformat],
	    [['start','section_id'], 'safe']
        ];
    }
    public function behaviors()
    {
	return [
	    [
        	'class' => \voskobovich\linker\LinkerBehavior::className(),
        	'relations' => [
        	    'whoteach_ids' => 'whoteach',
		    'whomteach_ids' => 'whomteach',
		    'whatteach_ids' => 'whatteach',
		    'howteach_ids' => 'howteach',

		    'categories_ids' => 'categories',
		    'programminglanguages_ids' => 'programmingLanguages',
        	],
	    ],
	];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'city_id' => Yii::t('app','City ID'),
            'section_id' => Yii::t('app','Section ID'),
            'company_id' => Yii::t('app','Company ID'),
            'name' => Yii::t('app','Name'),
            'description' => Yii::t('app','Description'),
            'start' => Yii::t('app','Start'),
            'start_str' => Yii::t('app','Start'),
	    'start_comment' => Yii::t('app','Comment'),
            'duration' => Yii::t('app','Duration'),
            'schedule' => Yii::t('app','Schedule'),
            'keywords' => Yii::t('app','Keywords'),
	    'price' => Yii::t('app','Price'),
	    'price_personal' => Yii::t('app','Price personal'),
	    'company' => Yii::t('app','Company'),
	    'url' => Yii::t('app','Link to website'),
        ];
    }
    public function beforeSave($insert = true){
	$t = strtotime($this->start_str);
	if($t !== FALSE){
	    $this->start = $t;
	    return parent::beforeSave($insert);
	}
	else{
	    $this->addError('start_str',"Can't convert 'start' to time");
	    return false;
	}
    }
    public function afterFind(){
	parent::afterFind();
	$this->start_str = date($this->PHPtimeformat,$this->start);
    }
    public function setStartDate($start_unix_timestamp){
	$this->start = intval($start_unix_timestamp);
	$this->start_str = date($this->PHPtimeformat,$this->start);
    }

    public function getWhoteach()
    {
	return $this->hasMany(Whoteach::className(),['id'=>'whoteach_id'])->viaTable('courses_whoteach',['course_id'=>'id']);
    }
    public function getWhomteach()
    {
	return $this->hasMany(Whomteach::className(),['id'=>'whomteach_id'])->viaTable('courses_whomteach',['course_id'=>'id']);
    }
    public function getWhatteach()
    {
	return $this->hasMany(Whatteach::className(),['id'=>'whatteach_id'])->viaTable('courses_whatteach',['course_id'=>'id']);
    }
    public function getHowteach()
    {
	return $this->hasMany(Howteach::className(),['id'=>'howteach_id'])->viaTable('courses_howteach',['course_id'=>'id']);
    }

    public function getCourseCategories()
    {
	return $this->hasMany(CategoriesCourses::className(),['course_id'=>'id']);
    }
    public function getCategories()
    {
	return $this->hasMany(Categories::className(),['id'=>'category_id'])->via('courseCategories');
    }

    public function getCourseProgrammingLanguages()
    {
	return $this->hasMany(CoursesProgrammingLanguages::className(),['course_id'=>'id']);
    }
    public function getProgrammingLanguages()
    {
	return $this->hasMany(ProgrammingLanguages::className(),['id'=>'programming_language_id'])->via('courseProgrammingLanguages');
    }

    public function getCity()
    {
	return $this->hasOne(Cities::className(),['id'=>'city_id']);
    }
    public function getSection()
    {
	return $this->hasOne(Sections::className(),['id'=>'section_id']);
    }
    public function getCompany()
    {
	return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
