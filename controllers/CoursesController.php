<?php

namespace app\controllers;

use Yii;
use app\models\Courses;
use app\models\CoursesSearch;
use app\models\Sections;
use app\models\Categories;
use app\models\Cities;
use app\models\Companies;
use app\models\ProgrammingLanguages;

use app\models\Whoteach;
use app\models\Whatteach;
use app\models\Whomteach;
use app\models\Howteach;
use app\models\TeachCategories;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

// use https://github.com/JiSoft/yii2-sypexgeo or following manual implementation
	private function guessClientCityByIp(){		
		function getClientIp() {
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
				$ipaddress = null;
			return $ipaddress;
		}

		function recognizeCity($city){
			$cities = ['dnipro'=>'dnepr'];
			return isset($cities[strtolower($city)])? $cities[strtolower($city)]:strtolower($city);
		}
		//check cookie
		
		$cityFromIp = '';
		$ip_str = getClientIp();
		if($ip_str){
			$json = '';
			$ips = explode(',', $ip_str);
			foreach($ips as $ip){
			$ip = trim($ip);
			try{
			    $json = file_get_contents('http://freegeoip.net/json/'.$ip);
			    if($json){
				    $location = json_decode($json);
				    if($location){
					$cityFromIp = $location->city;
					if($cityFromIp){
					    break;//break from foreach
					}
				    }
			    }
			}
			catch(Exception $e){
			    error_log('failed to get or parse city by ip '.$e->getMessage());
			}
			}
		}
		return ($cityFromIp!=null && $cityFromIp != '') ? recognizeCity($cityFromIp) : null;
	}
	private function getCityFromCookie(){
		return isset(Yii::$app->request->cookies['clientCity']) ? Yii::$app->request->cookies['clientCity']->value : null;
	}
	private function setCityToCookie($city){
		Yii::$app->response->cookies->add(new \yii\web\Cookie([
			'name' => 'clientCity',
			'value' => $city,
			'domain' => SITE_NAME,
			'expire' => time() + 100*24*60*60,//100 days
		]));
	}
	private function getSectionFromCookie(){
		return isset(Yii::$app->request->cookies['clientSection']) ? Yii::$app->request->cookies['clientSection']->value : null;
	}
	private function setSectionToCookie($section){
		Yii::$app->response->cookies->add(new \yii\web\Cookie([
			'name' => 'clientSection',
			'value' => $section,
			'domain' => SITE_NAME,
			'expire' => time() + 100*24*60*60,//100 days
		]));
	}
    /**
     * Lists all Courses models and allows searching.
     * @return mixed
     */
    public function actionIndex($whatteach=null, $whoteach=null, $whomteach=null, $howteach=null, $timeteach=null, $city=null, $section=null) //whatteach, whoteach, whatteach, howteach
    {
		$cityGuessed = false;
		if($city){
		    $clientCity = $city;
		}
		else{
		$clientCity = $this->getCityFromCookie();
		if(!$clientCity){
		    $clientCity = $this->guessClientCityByIp();
		    $cityGuessed = !is_null($clientCity) && !empty($clientCity);
		}
		}
		$this->setCityToCookie($clientCity);
		$this->view->params['clientCity'] = $clientCity;
		
		if(is_null($section)){
		    $section = $this->getSectionFromCookie();
		}
		$section_orm = Sections::findOne($section);
		if($section_orm){
		    $this->setSectionToCookie($section);
		    $this->view->params['clientSection'] = $section_orm->name;
		}
		else{
		    $this->setSectionToCookie(0);
		    $section = 0;
		    $this->view->params['clientSection'] = 'All';
		}
		
		if($whatteach){
			$whatteach = split(',',$whatteach);
			foreach($whatteach as &$el){
				$el = intval($el);
			}
		}
		if($whoteach){
			$whoteach = split(',',$whoteach);
			foreach($whoteach as &$el){
				$el = intval($el);
			}
		}
		if($whomteach){
			$whomTeach = split(',',$whomteach);
			foreach($whomteach as &$el){
				$el = intval($el);
			}
		}
		if($howteach){
			$howteach = split(',',$howteach);
			foreach($howteach as &$el){
				$el = intval($el);
			}
		}
		if($timeteach){
			$timeteach = split(',',$timeteach);
			foreach($timeteach as &$el){
				$el = intval($el);
			}
		}

        $searchModel = new CoursesSearch();
        
	$queryParams = Yii::$app->request->queryParams;
	unset($queryParams['sity']);
	unset($queryParams['section']);
	$city = Cities::find()->where(['urlprefix'=>$clientCity])->one();
	if($city && !isset($queryParams['city_id'])){
		$queryParams['city_id'] = $city->id;
	}
	if($section && !isset($queryParams['section_id'])){
		$queryParams['section_id'] = $section;
		if(is_array($whatteach)){
		    $whatteach[] = $section;
		}
		else{
		    $whatteach = [$section];
		}
	}
	$queryParams['whatteach'] = $whatteach;
	$queryParams['whoteach'] = $whoteach;
	$queryParams['whomteach'] = $whomteach;
	$queryParams['howteach'] = $howteach;
	$queryParams['timeteach'] = $timeteach;
        $dataProvider = $searchModel->search($queryParams);
    
	//$sections = Sections::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'cityGuessed' => $cityGuessed,
	    //'sections' => ArrayHelper::map($sections,'id','name'),
		'selectedCategories'=>[
		'whatteach' => $whatteach,
		'whoteach' => $whoteach,
		'whomteach' => $whomteach,
		'howteach' => $howteach],
		'teachCategories' => TeachCategories::CATEGORIES,
        ]);
    }

    /**
     * Lists all Courses models and allows editing and adding.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new CoursesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Courses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionDetails($id)
    {
        return $this->render('details', [
            'model' => $this->findModel($id),
        ]);
    }

    private function getVocabularies()
    {
	return [
	    'cities' => ArrayHelper::map(Cities::find()->select('id,name')->all(),'id','name'),
	    'sections' => ArrayHelper::map(Sections::find()->select('id,name')->all(),'id','name'),
	    'companies' => ArrayHelper::map(Companies::find()->select('id,name')->all(),'id','name'),
	    'categories' => ArrayHelper::map(Categories::find()->select('id,name')->all(),'id','name'),
	    'programming_languages' => ArrayHelper::map(ProgrammingLanguages::find()->select('id,name')->where(['enabled'=>true])->all(),'id','name'),

	    'whoteach' => ArrayHelper::map(Whoteach::find()->select('id,name')->all(),'id','name'),
	    'whatteach' => ArrayHelper::map(Whatteach::find()->select('id,name')->all(),'id','name'),
	    'whomteach' => ArrayHelper::map(Whomteach::find()->select('id,name')->all(),'id','name'),
	    'howteach' => ArrayHelper::map(Howteach::find()->select('id,name')->all(),'id','name'),
	];
    }
/*
    private function linkCategories($course, $categories){
	if($categories){
	    foreach($categories as $category){
		$category_model = Categories::findOne($category);
		if($category_model){
		    $course->link('categories',$category_model);
		}
	    }
	}
    }
    private function linkProgrammingLanguages($course, $programming_languages){
	if($programming_languages){
	    foreach($programming_languages as $language){
		$lang_model = ProgrammingLanguages::findOne($language);
		if($lang_model){
		    $course->link('programmingLanguages',$lang_model);
		}
	    }
	}
    }

    private function linkWhoteach($course, $whoteach){
	if($whoteach){
	    $classname = 'Whoteach';//http://stackoverflow.com/questions/3354628/from-the-string-name-of-a-class-can-i-get-a-static-variable
	    foreach($whoteach as $el){
		$model = $classname::findOne($el);
		if($model){
		    $course->link('whoteach',$model);
		}
	    }
	}
    }*/

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Courses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    /*
	    $categories = Yii::$app->request->post('Courses')['categories'];
	    $programming_languages = Yii::$app->request->post('Courses')['programmingLanguages'];
	    $whoteach = Yii::$app->request->post('Courses')['whoteach'];

//	    $transaction = Yii::$app->db->beginTransaction();
	    try{
		$this->linkCategories($model, $categories);
		$this->linkProgrammingLanguages($model, $programming_languages);
		$this->linkWhoteach($model, $whoteach);
//		$transaction->commit();
	    }
	    catch(Exception $e){
//		$transaction->rollBack();
		return $this->redirect(['error/index', 'message'=>$e->getMessage()]);
	    }
	    */
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
	    if($model->start == 0){
		$model->setStartDate(time());
	    }
            return $this->render('create', [
                'model' => $model,'vocabularies'=>$this->getVocabularies()
            ]);
        }
    }

    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    /*
	    $categories = Yii::$app->request->post('Courses')['categories'];
	    $programming_languages = Yii::$app->request->post('Courses')['programmingLanguages'];
	    $whoteach = Yii::$app->request->post('Courses')['whoteach'];
//	    $transaction = Yii::$app->db->beginTransaction();
	    try{
		$this->linkCategories($model, $categories);
		$this->linkProgrammingLanguages($model, $programming_languages);
		$this->linkWhoteach($model, $whoteach);
//		$transaction->commit();
	    }
	    catch(Exception $e){
//		$transaction->rollBack();
		return $this->redirect(['error/index', 'message'=>$e->getMessage()]);
	    }
	    */
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'vocabularies'=>$this->getVocabularies()
            ]);
        }
    }

    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::find()->where(['id'=>$id])->with('city')->with('section')->with('company')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
