<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ContactsLog;
use app\models\SubscribeForm;
use app\models\Subscriptions;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = "landing";
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            $sendResult = $model->contact(Yii::$app->params['adminEmail']);//if it is send somewhere or stored to file by default
            //save name, email, subject, body
            $contactsLog = new ContactsLog();
            $contactsLog->name = $model->name;
            $contactsLog->email = $model->email;
            $contactsLog->subject = $model->subject;
            $contactsLog->body = $model->body;
            $sendResult |= $contactsLog->save();
            if($sendResult) {
                Yii::$app->session->setFlash('contactFormSubmitted');
            }
	    
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionSubscribe()
    {
        $model = new SubscribeForm();
        if($model->load(Yii::$app->request->post())){
	    $model->confirm(Yii::$app->params['adminEmail'],Yii::$app->params['email_from']);
	    $subscription = new Subscriptions();
	    $subscription->email = $model->email;
	    $subscription->ts = time();
	    if($subscription->save()){
        	Yii::$app->session->setFlash('subscribeFormSubmitted','success');
	    }
	    else{
        	Yii::$app->session->setFlash('subscribeFormSubmitted',Yii::t('Failed to subscribe, please try once more'));
	    }
        }
        return $this->render('subscribe',['model'=>$model]);
    }
    public function actionFaq()
    {
        return $this->render('faq');
    }
}
